package com.t1.alieva.tm.command.user;


import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.UserNotFoundException;
import com.t1.alieva.tm.model.User;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserCommand extends AbstractCommand {

    public void showUser(@Nullable final User user) throws AbstractEntityNotFoundException {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @Nullable
    public String getArgument() {
        return null;
    }


}
