package com.t1.alieva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getName() {
        return "exit";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Close Application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
