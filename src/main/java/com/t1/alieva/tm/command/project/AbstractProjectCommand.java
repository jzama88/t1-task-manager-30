package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.api.service.IProjectService;
import com.t1.alieva.tm.api.service.IProjectTaskService;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }
}
