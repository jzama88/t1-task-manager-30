package com.t1.alieva.tm.exception.user;

import com.t1.alieva.tm.exception.AbstractException;

import javax.naming.NamingSecurityException;

public final class AuthenticationException extends NamingSecurityException {

    public AuthenticationException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }
}
