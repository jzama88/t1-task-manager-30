package com.t1.alieva.tm.component;

import com.t1.alieva.tm.command.data.AbstractDataCommand;
import com.t1.alieva.tm.command.data.DataBackupLoadCommand;
import com.t1.alieva.tm.command.data.DataBackupSaveCommand;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.CommandNotSupportedException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.naming.AuthenticationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;


    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() throws
            AbstractException,
            ClassNotFoundException,
            AuthenticationException,
            IOException{
        load();
        start();
    }

    public void save() throws
            AbstractException,
            ClassNotFoundException,
            AuthenticationException,
            IOException
    {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() throws
            AbstractException,
            ClassNotFoundException,
            AuthenticationException,
            IOException
    {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            save();
        }

    }
}
