package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IRepository;
import com.t1.alieva.tm.model.AbstractModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return models;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return models
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        this.models.addAll(models);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        removeAll();
        return add(models);
    }

    @Override
    public void removeAll() {
        models.clear();
    }

    @Override
    @Nullable
    public M findOneById(@Nullable final String id) {
        return models
                .stream()
                .filter(r -> id.equals(r.getId()))
                .findFirst().orElse(null);
    }

    @Override
    @NotNull
    public M findOneByIndex(@Nullable final Integer index) {
        return models.get(index);
    }

    @Override
    @Nullable
    public M removeOne(@Nullable final M model) {
        models.remove(model);
        return model;
    }

    @Override
    @Nullable
    public M removeOneById(@Nullable final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public void removeAll(@NotNull final Collection<M> models) {
        models.removeAll(models);
    }

    @Override
    @NotNull
    public M removeOneByIndex(
            @Nullable final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return findOneById(id) != null;
    }
}
