package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.api.repository.ICommandRepository;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.exception.system.ArgumentNotSupportedException;
import com.t1.alieva.tm.exception.system.CommandNotSupportedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandService extends ICommandRepository {

}
